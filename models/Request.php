<?php

namespace app\models;

use app\models\Transfer;
use Yii;
use yii\base\Behavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "request".
 *
 * @property int $id
 * @property int|null $employee_id
 * @property int|null $project_id
 * @property int|null $location_id
 * @property int|null $department_id
 * @property int|null $status
 * @property string|null $content
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Department $department
 * @property Employee $employee
 * @property Location $location
 * @property Project $project
 */


class Request extends \yii\db\ActiveRecord
{
	const APPROVE= 1;
	const DENY = 0;
	const PENDING = 2;

	const STATUS  = [
		self::DENY    => 'Deny',
		self::APPROVE => 'Approve',
		self::PENDING => 'Pending',
	];

	/**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee_id', 'project_id', 'location_id', 'department_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['content'], 'string'],
	        [[ 'project_id', 'location_id', 'department_id', 'status',], 'required'],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => Location::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee',
            'project_id' => 'Project',
            'location_id' => 'Location',
            'department_id' => 'Department',
            'status' => 'Status',
            'content' => 'Content',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
	public function behaviors() {
		// TODO: Change the auto generated stub
		return [
			'timestamp' => [
				'class' => TimestampBehavior::class
			]
		];
	}

	/**
     * Gets query for [[Department]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * Gets query for [[Location]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id' => 'location_id']);
    }

    /**
     * Gets query for [[Project]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
    function afterSave($insert, $changedAttributes) {
	    // TODO: Change the auto generated stub
//	    echo 34234234;

	    $transfer = Transfer::findOne(['request_id' => $this->primaryKey]);
	    if(!empty($transfer)) {
	    	$transfer->updateAttributes([
	    		'transfer_from_project_id' => $this->project_id,
	    		'transfer_from_location_id' => $this->location_id,
	    		'transfer_from_department_id' => $this->department_id,
		    ]);
	    } else {

		    $transfer       = new Transfer();
		    $transfer->employee_id = $this->employee_id;
		    $transfer->request_id = $this->primaryKey;
		    $transfer->status = $this->status;
		    $transfer->transfer_from_project_id = $this->project_id;
		    $transfer->transfer_from_location_id = $this->location_id;
		    $transfer->transfer_from_department_id = $this->department_id;
		    $transfer->save();
	    }

	    parent::afterSave($insert, $changedAttributes);
    }
}
