<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "employee".
 *
 * @property int         $id
 * @property string|null $employee_name
 * @property int|null    $user_id
 * @property string|null $role
 * @property string|null $work_experience
 * @property int|null    $current_project_id
 * @property int|null    $current_location_id
 * @property int|null    $current_department_id
 * @property string|null $employee_first_name
 * @property string|null $day_of_birth
 * @property int|null    $sex
 * @property string|null $address
 * @property string|null $phone_num
 * @property int|null    $created_at
 * @property int|null    $updated_at
 *
 * @property Department  $currentDepartment
 * @property Location    $currentLocation
 * @property Project     $currentProject
 * @property User        $user
 * @property Request[]   $requests
 * @property Transfer[]  $transfers
 */
class Employee extends \yii\db\ActiveRecord {

	const MALE   = 1;

	const FEMALE = 0;

	const GAY    = 2;

	const LESS   = 3;

	const SEX    = [
		self::MALE   => 'Male',
		self::FEMALE => 'Female',
		self::GAY    => 'Gay',
		self::LESS   => 'Less',
	];

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'employee';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'user_id',
					'current_project_id',
					'current_location_id',
					'current_department_id',
					'phone_num',
					'sex',
					'created_at',
					'updated_at',
				],
				'integer',
			],
			[
				[
					'work_experience',
					'address',
				],
				'string',
			],
			[
				[
					'employee_name',
					'user_id',
					'role',
					'current_project_id',
					'current_location_id',
					'current_department_id',
					'sex',
					'day_of_birth',
					'phone_num',
				],
				'required',
			],
			[
				['employee_first_name'],
				'safe',
			],
			[
				[
					'employee_name',
					'employee_first_name',
					'phone_num',
				],
				'string',
				'max' => 15,
			],
			[
				['role'],
				'string',
				'max' => 50,
			],
			[
				['current_department_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Department::className(),
				'targetAttribute' => ['current_department_id' => 'id'],
			],
			[
				['current_location_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Location::className(),
				'targetAttribute' => ['current_location_id' => 'id'],
			],
			[
				['current_project_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Project::className(),
				'targetAttribute' => ['current_project_id' => 'id'],
			],
			[
				['user_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => User::className(),
				'targetAttribute' => ['user_id' => 'id'],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'                    => 'ID',
			'employee_name'         => 'Employee Name',
			'user_id'               => 'User ID',
			'role'                  => 'Role',
			'work_experience'       => 'Work Experience',
			'current_project_id'    => 'Current Project ',
			'current_location_id'   => 'Current Location ',
			'current_department_id' => 'Current Department',
			'employee_first_name'   => 'Employee First Name',
			'day_of_birth'          => 'Day Of Birth',
			'sex'                   => 'Sex',
			'address'               => 'Address',
			'phone_num'             => 'Phone Num',
			'created_at'            => 'Created At',
			'updated_at'            => 'Updated At',
		];
	}

	public function behaviors() {
		// TODO: Change the auto generated stub
		return [
			'timestamp' => [
				'class' => TimestampBehavior::class,
			],
		];
	}

	/**
	 * Gets query for [[CurrentDepartment]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCurrentDepartment() {
		return $this->hasOne(Department::className(), ['id' => 'current_department_id']);
	}

	/**
	 * Gets query for [[CurrentLocation]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCurrentLocation() {
		return $this->hasOne(Location::className(), ['id' => 'current_location_id']);
	}

	/**
	 * Gets query for [[CurrentProject]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCurrentProject() {
		return $this->hasOne(Project::className(), ['id' => 'current_project_id']);
	}

	/**
	 * Gets query for [[User]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	 * Gets query for [[Requests]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getRequests() {
		return $this->hasMany(Request::className(), ['employee_id' => 'id']);
	}

	/**
	 * Gets query for [[Transfers]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getTransfers() {
		return $this->hasMany(Transfer::className(), ['employee_id' => 'id']);
	}

	public function beforeSave($insert) {
		$this->day_of_birth = strtotime($this->day_of_birth);
		return parent::beforeSave($insert);
	}
}
