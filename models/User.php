<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property int $role
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Employee[] $employees
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
	const ADMIN = 1;
	const USER = 2;
	const ROLE = [self::ADMIN => 'Admin', self::USER => 'User'];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password', 'role'], 'required'],
            [['role', 'created_at', 'updated_at'], 'integer'],
            [['username'], 'string', 'max' => 255],
            [['password'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'role' => 'Role',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function behaviors() {
	    // TODO: Change the auto generated stub
	    return [
	    	'timestamp' => array(
	    		'class' => TimestampBehavior::classname()

		    )
	    ];
    }

	/**
     * Gets query for [[Employees]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployees()
    {
        return $this->hasMany(Employee::className(), ['user_id' => 'id']);
    }
	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}
	public function validatePassword($password)
	{
		return Yii::$app->security->validatePassword($password, $this->password);

	}
	public static function findIdentity($id) {
		// TODO: Implement findIdentity() method.
		return static::findOne($id);
	}

	public static function findIdentityByAccessToken($token, $type = null) {
		// TODO: Implement findIdentityByAccessToken() method.
	}

	public function getId() {
		// TODO: Implement getId() method.
		return $this->id;
	}

	public function getAuthKey() {
		// TODO: Implement getAuthKey() method.
	}

	public function validateAuthKey($authKey) {
		// TODO: Implement validateAuthKey() method.
	}
}
