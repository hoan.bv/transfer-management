<?php

namespace app\models;

use app\models\Request;
use kartik\daterange\DateRangeBehavior;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SearchRequest represents the model behind the search form of `app\models\Request`.
 */
class SearchRequest extends Request
{
	public $createTimeStart;

	public $createTimeEnd;
	public function behaviors() {
		return [
			[
				'class'              => DateRangeBehavior::class,
				'attribute'          => 'created_at',
				'dateStartAttribute' => 'createTimeStart',
				'dateEndAttribute'   => 'createTimeEnd',
			],
		];
	}
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'employee_id', 'project_id', 'location_id', 'department_id', 'status',  'updated_at'], 'integer'],
            [['content'], 'safe'],
	        [
		        ['created_at'],
		        'match',
		        'pattern' => '/^.+\s\-\s.+$/',
	        ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

	    if(Yii::$app->user->identity->role != User::ADMIN){
		    $id_employee = Employee::findOne(['user_id' => Yii::$app->user->identity->id])->id;
		    $query = Request::find()->where(['employee_id' => $id_employee]);
	    }else{
            $query = Request::find();
	    }


	    // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions

		    $query->andFilterWhere([
			    'id'            => $this->id,
			    'employee_id'   => $this->employee_id,
			    'project_id'    => $this->project_id,
			    'location_id'   => $this->location_id,
			    'department_id' => $this->department_id,
			    'status'        => $this->status,
			    'content'       => $this->content,

		    ])->andFilterWhere([
			    '>=',
			    'created_at',
			    $this->createTimeStart,
		    ])->andFilterWhere([
			    '<=',
			    'created_at',
			    $this->createTimeEnd,
		    ]);
        return $dataProvider;
    }
}
