<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "transfer".
 *
 * @property int $id
 * @property int|null $employee_id
 * @property int|null $transfer_to_project_id
 * @property int|null $project
 * @property int|null $transfer_to_location_id
 * @property int|null $transfer_to_department_id
 * @property int|null $transfer_from_project_id
 * @property int|null $transfer_from_location_id
 * @property int|null $transfer_from_department_id
 * @property string $transfer_joining_date
 * @property int $status
 * @property string $transfer_relieving_date
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $request_id
 *
 * @property Employee $employee
 * @property Request $request
 */
class Transfer extends \yii\db\ActiveRecord
{
	const APPROVE= 1;
	const DENY = 0;
	const PENDING = 2;

	const STATUS  = [
		self::DENY    => 'Deny',
		self::APPROVE => 'Approve',
		self::PENDING => 'Pending',
	];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transfer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee_id','request_id', 'transfer_to_project_id', 'transfer_to_location_id', 'transfer_to_department_id', 'transfer_from_project_id', 'transfer_from_location_id', 'transfer_from_department_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['employee_id', 'transfer_from_project_id', 'transfer_from_location_id', 'transfer_from_department_id', 'status'], 'required'],
            [['transfer_joining_date', 'transfer_relieving_date'], 'safe'],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
	        [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Request::className(), 'targetAttribute' => ['request_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee ',
            'request_id' => 'Request ID',
            'transfer_to_project_id' => 'Transfer To Project ',
            'transfer_to_location_id' => 'Transfer To Location ',
            'transfer_to_department_id' => 'Transfer To Department ',
            'transfer_from_project_id' => 'Transfer From Project ',
            'transfer_from_location_id' => 'Transfer From Location ',
            'transfer_from_department_id' => 'Transfer From Department',
            'transfer_joining_date' => 'Transfer Joining Date',
            'status' => 'Status',
            'transfer_relieving_date' => 'Transfer Relieving Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
	public function behaviors() {
		return [
			'timestamp' => [
				'class' => TimestampBehavior::class
			]
		];
	}
    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::class, ['id' => 'employee_id']);
    }
	public function getToLocation()
	{
		return $this->hasOne(Location::class, ['id' => 'transfer_to_location_id']);
	}
	public function getFromLocation()
	{
		return $this->hasOne(Location::class, ['id' => 'transfer_from_location_id']);
	}
	public function getToProject()
	{
		return $this->hasOne(Project::class, ['id' => 'transfer_to_project_id']);
	}
	public function getFromProject()
	{
		return $this->hasOne(Project::class, ['id' => 'transfer_from_project_id']);
	}
	public function getToDepartment()
	{
		return $this->hasOne(Department::class, ['id' => 'transfer_to_department_id']);
	}
	public function getFromDepartment()
	{
		return $this->hasOne(Department::class, ['id' => 'transfer_from_department_id']);
	}
	public function getRequest()
	{
		return $this->hasOne(Request::className(), ['id' => 'request_id']);
	}

	public function beforeSave($insert)
	{
		$this->transfer_joining_date = strtotime($this->transfer_joining_date);
		$this->transfer_relieving_date = strtotime($this->transfer_relieving_date);
		return parent::beforeSave($insert);
	}
}
