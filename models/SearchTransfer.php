<?php

namespace app\models;

use app\models\Transfer;
use kartik\daterange\DateRangeBehavior;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SearchTransfer represents the model behind the search form of `app\models\Transfer`.
 */
class SearchTransfer extends Transfer
{
    /**
     * {@inheritdoc}
     */
	public $createTimeStart;
	public $createTimeEnd;
	public $joiningTimeStart;
	public $joiningTimeEnd;
	public $relievingTimeStart;
	public $relievingTimeEnd;

	public function behaviors() {
		return [
			[
				'class'              => DateRangeBehavior::class,
				'attribute'          => 'created_at',
				'dateStartAttribute' => 'createTimeStart',
				'dateEndAttribute'   => 'createTimeEnd',
			],
			[
				'class'              => DateRangeBehavior::class,
				'attribute'          => 'transfer_joining_date',
				'dateStartAttribute' => 'joiningTimeStart',
				'dateEndAttribute'   => 'joiningTimeEnd',
			],
			[
				'class'              => DateRangeBehavior::class,
				'attribute'          => 'transfer_relieving_date',
				'dateStartAttribute' => 'relievingTimeStart',
				'dateEndAttribute'   => 'relievingTimeEnd',
			],
		];
	}
    public function rules()
    {
        return [
            [['id', 'employee_id','request_id', 'transfer_to_project_id', 'transfer_to_location_id', 'transfer_to_department_id', 'transfer_from_project_id', 'transfer_from_location_id', 'transfer_from_department_id', 'status', 'updated_at'], 'integer'],
            [['transfer_joining_date', 'transfer_relieving_date'], 'safe'],
	        [
		        ['transfer_joining_date','created_at', 'transfer_relieving_date'],
		        'match',
		        'pattern' => '/^.+\s\-\s.+$/',
	        ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
	    if(Yii::$app->user->identity->role != User::ADMIN) {
			$id_employee = Employee::findOne(['user_id' => Yii::$app->user->identity->id])->id;
		    $query = Transfer::find()->where(['employee_id' => $id_employee,'status' => Transfer::PENDING]);
	    } else {

        $query = Transfer::find();
	    }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'employee_id' => $this->employee_id,
            'request_id' => $this->request_id,
            'transfer_to_project_id' => $this->transfer_to_project_id,
            'transfer_to_location_id' => $this->transfer_to_location_id,
            'transfer_to_department_id' => $this->transfer_to_department_id,
            'transfer_from_project_id' => $this->transfer_from_project_id,
            'transfer_from_location_id' => $this->transfer_from_location_id,
            'transfer_from_department_id' => $this->transfer_from_department_id,
            'status' => $this->status,
        ])->andFilterWhere([
	        '>=',
	        'created_at',
	        $this->createTimeStart,
        ])->andFilterWhere([
	        '<=',
	        'created_at',
	        $this->createTimeEnd,
        ])->andFilterWhere([
	        '>=',
	        'transfer_joining_date',
	        $this->joiningTimeStart,
        ])->andFilterWhere([
	        '<=',
	        'transfer_joining_date',
	        $this->joiningTimeEnd,
        ])->andFilterWhere([
	        '>=',
	        'transfer_relieving_date',
	        $this->relievingTimeStart,
        ])->andFilterWhere([
	        '<=',
	        'transfer_relieving_date',
	        $this->relievingTimeEnd,
        ]);

        return $dataProvider;
    }
}
