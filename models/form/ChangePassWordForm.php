<?php
/**
 * Created by Navatech.
 * @project transfer-management
 * @author  hoan
 * @email   hoannn.bv[at]gmail.com
 * @date    2/2/2021
 * @time    10:32 AM
 */

namespace app\models\form;

use app\models\User;
use Yii;
use yii\base\Model;

class ChangePassWordForm extends Model {

	public $username;

	public $old_password;

	public $new_password;

	public $role;

	public function rules() {
		return [
			[
				[
					'username',
					'old_password',
					'new_password',
					'role',
				],
				'required',
			],
			[
				'old_password',
				function() {
					if ($this->old_password != '') {
						$user = User::findOne(\Yii::$app->user->identity->id);
						if ($user === null || !\Yii::$app->security->validatePassword($this->old_password, $user->password)) {
							$this->addError('old_password', 'Password does not match');
						}
					}
				},
			],
		];
	}

	public function changePassword() {
		if (!$this->validate()) {
			return false;
		}
		$user = User::findOne(\Yii::$app->user->identity->id);
		$user->updateAttributes(['password' => \Yii::$app->getSecurity()->generatePasswordHash($this->new_password)]);
		return true;
	}
}
