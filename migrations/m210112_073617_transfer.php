<?php

use yii\db\Schema;
use yii\db\Migration;

class m210112_073617_transfer extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%transfer}}',
            [
                'id'=> Schema::TYPE_PK.'',
                'employee_id'=> Schema::TYPE_INTEGER.'(11)',
                'transfer_to_project_id'=> Schema::TYPE_INTEGER.'(11)',
                'transfer_to_location_id'=> Schema::TYPE_INTEGER.'(11)',
                'transfer_to_department_id'=> Schema::TYPE_INTEGER.'(11)',
                'transfer_from_project_id'=> Schema::TYPE_INTEGER.'(11)',
                'transfer_from_location_id'=> Schema::TYPE_INTEGER.'(11)',
                'transfer_from_department_id'=> Schema::TYPE_INTEGER.'(11)',
                'transfer_joining_date'=> Schema::TYPE_INTEGER.' NOT NULL',
                'status'=> Schema::TYPE_INTEGER.'(2) NOT NULL DEFAULT "0"',
                'transfer_relieving_date'=> Schema::TYPE_INTEGER.' NOT NULL',
                'created_at'=> Schema::TYPE_INTEGER.'(11)'. 'DEFAULT null',
                'updated_at'=> Schema::TYPE_INTEGER.'(11)'. 'DEFAULT null',
                ],
            $tableOptions
        );

        $this->createIndex('transfer-employee', '{{%transfer}}','employee_id',0);
    }

    public function safeDown()
    {
        $this->dropIndex('transfer-employee', '{{%transfer}}');
        $this->dropTable('{{%transfer}}');
    }
}
