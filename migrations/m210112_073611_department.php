<?php

use yii\db\Schema;
use yii\db\Migration;

class m210112_073611_department extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%department}}',
            [
                'id'=> Schema::TYPE_PK.'',
                'department_name'=> Schema::TYPE_STRING.'(50)',
                'description'=> Schema::TYPE_STRING.'(50)',
                'created_at'=> Schema::TYPE_INTEGER.'(11)'. 'DEFAULT NULL',
                'updated_at'=> Schema::TYPE_INTEGER.'(11)',
                ],
            $tableOptions);
	        $this->execute("
			INSERT INTO `department` (`id`, `department_name`, `description`) VALUES
				(1, 'Director', 'director'),
				(2, 'Accounting', 'accounting'),
				(3, 'Sales', 'sales'),
				(4, 'Teachnical', 'teachnical'),
				(5, 'Support', 'support'),
				(6, 'Marketing', 'marketing');"

        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%department}}');
    }
}
