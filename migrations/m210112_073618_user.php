<?php

use yii\db\Schema;
use yii\db\Migration;

class m210112_073618_user extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%user}}',
            [
                'id'=> Schema::TYPE_PK.'',
                'username'=> Schema::TYPE_STRING.'(255) NOT NULL',
                'password'=> Schema::TYPE_STRING.'(255) NOT NULL',
                'role'=> Schema::TYPE_INTEGER.'(11) NOT NULL',
                'created_at'=> Schema::TYPE_INTEGER.'(11)'. 'DEFAULT null',
                'updated_at'=> Schema::TYPE_INTEGER.'(11)'. 'DEFAULT null',
                ],
            $tableOptions
        );
	    $this->insert('{{%user}}', [
		    'id'         => '1',
		    'username'   => 'admin',
		    'password'   => \Yii::$app->getSecurity()->generatePasswordHash('123'),
		    'role'       => 1,
		    'created_at' => '1607421343',
		    'updated_at' => '1607421500',
	    ]);

    }

    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
