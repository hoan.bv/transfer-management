<?php

use yii\db\Schema;
use yii\db\Migration;

class m210112_073619_Relations extends Migration
{
    public function safeUp()
    {
                                $this->addForeignKey('fk_employee_current_department_id', '{{%employee}}', 'current_department_id', 'department', 'id');
            $this->addForeignKey('fk_employee_current_location_id', '{{%employee}}', 'current_location_id', 'location', 'id');
            $this->addForeignKey('fk_employee_current_project_id', '{{%employee}}', 'current_project_id', 'project', 'id');
            $this->addForeignKey('fk_employee_user_id', '{{%employee}}', 'user_id', 'user', 'id');
                                                            $this->addForeignKey('fk_request_department_id', '{{%request}}', 'department_id', 'department', 'id');
            $this->addForeignKey('fk_request_employee_id', '{{%request}}', 'employee_id', 'employee', 'id');
            $this->addForeignKey('fk_request_location_id', '{{%request}}', 'location_id', 'location', 'id');
            $this->addForeignKey('fk_request_project_id', '{{%request}}', 'project_id', 'project', 'id');
                        $this->addForeignKey('fk_transfer_employee_id', '{{%transfer}}', 'employee_id', 'employee', 'id');
                            }

    public function safeDown()
    {

                           $this->dropForeignKey('fk_employee_current_department_id', '{{%employee}}');
           $this->dropForeignKey('fk_employee_current_location_id', '{{%employee}}');
           $this->dropForeignKey('fk_employee_current_project_id', '{{%employee}}');
           $this->dropForeignKey('fk_employee_user_id', '{{%employee}}');
                                                           $this->dropForeignKey('fk_request_department_id', '{{%request}}');
           $this->dropForeignKey('fk_request_employee_id', '{{%request}}');
           $this->dropForeignKey('fk_request_location_id', '{{%request}}');
           $this->dropForeignKey('fk_request_project_id', '{{%request}}');
                       $this->dropForeignKey('fk_transfer_employee_id', '{{%transfer}}');
                        
    }
}
