<?php

use yii\db\Schema;
use yii\db\Migration;

class m210112_073615_project extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%project}}',
            [
                'id'=> Schema::TYPE_PK.'',
                'project_name'=> Schema::TYPE_STRING.'(50)',
                'description'=> Schema::TYPE_STRING.'(50)',
                'created_at'=> Schema::TYPE_INTEGER.'(11)'. 'DEFAULT null',
                'updated_at'=> Schema::TYPE_INTEGER.'(11)'. 'DEFAULT null',
                ],
            $tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%project}}');
    }
}
