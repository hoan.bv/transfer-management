<?php

use yii\db\Schema;
use yii\db\Migration;

class m210112_073616_request extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%request}}',
            [
                'id'=> Schema::TYPE_PK.'',
                'employee_id'=> Schema::TYPE_INTEGER.'(11)',
                'project_id'=> Schema::TYPE_INTEGER.'(11)',
                'location_id'=> Schema::TYPE_INTEGER.'(11)',
                'department_id'=> Schema::TYPE_INTEGER.'(11)',
                'status'=> Schema::TYPE_INTEGER.'(2) DEFAULT "0"',
                'content'=> Schema::TYPE_TEXT.'',
                'created_at'=> Schema::TYPE_INTEGER.'(11)'. 'DEFAULT null',
                'updated_at'=> Schema::TYPE_INTEGER.'(11)'. 'DEFAULT null',
                ],
            $tableOptions
        );

        $this->createIndex('request-employee', '{{%request}}','employee_id',0);
        $this->createIndex('request-department', '{{%request}}','department_id',0);
        $this->createIndex('request-project', '{{%request}}','project_id',0);
        $this->createIndex('request-location', '{{%request}}','location_id',0);
    }

    public function safeDown()
    {
        $this->dropIndex('request-employee', '{{%request}}');
        $this->dropIndex('request-department', '{{%request}}');
        $this->dropIndex('request-project', '{{%request}}');
        $this->dropIndex('request-location', '{{%request}}');
        $this->dropTable('{{%request}}');
    }
}
