<?php

use yii\db\Schema;
use yii\db\Migration;

class m210112_073612_employee extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable(
            '{{%employee}}',
            [
                'id'=> Schema::TYPE_PK.'',
                'employee_name'=> Schema::TYPE_STRING.'(15)',
                'user_id'=> Schema::TYPE_INTEGER.'(11)',
                'role'=> Schema::TYPE_STRING.'(50)',
                'work_experience'=> Schema::TYPE_TEXT.'',
                'current_project_id'=> Schema::TYPE_INTEGER.'(11)',
                'current_location_id'=> Schema::TYPE_INTEGER.'(11)',
                'current_department_id'=> Schema::TYPE_INTEGER.'(11)',
                'employee_first_name'=> Schema::TYPE_STRING.'(15)',
                'day_of_birth'=> Schema::TYPE_INTEGER.'(11)',
                'sex'=> Schema::TYPE_INTEGER.'(2) DEFAULT "0"',
                'address'=> Schema::TYPE_TEXT.'',
                'phone_num'=> Schema::TYPE_STRING.'(15)',
                'created_at'=> Schema::TYPE_INTEGER.'(11)'. 'DEFAULT NULL',
                'updated_at'=> Schema::TYPE_INTEGER.'(11)'. 'DEFAULT NULL',
                ],
            $tableOptions
        );


        $this->createIndex('employee-user', '{{%employee}}','user_id',0);
        $this->createIndex('employee-department', '{{%employee}}','current_department_id',0);
        $this->createIndex('employee-location', '{{%employee}}','current_location_id',0);
        $this->createIndex('employee-project', '{{%employee}}','current_project_id',0);
    }

    public function safeDown()
    {
        $this->dropIndex('employee-user', '{{%employee}}');
        $this->dropIndex('employee-department', '{{%employee}}');
        $this->dropIndex('employee-location', '{{%employee}}');
        $this->dropIndex('employee-project', '{{%employee}}');
        $this->dropTable('{{%employee}}');
    }
}
