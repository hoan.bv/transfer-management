<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles adding columns to table `{{%transfer}}`.
 */
class m210203_033747_add_request_id_column_to_transfer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%transfer}}', 'request_id', 'INTEGER AFTER `employee_id`');
	    $this->addForeignKey('fk_transfer_request', '{{%transfer}}', 'request_id', 'request', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%transfer}}', 'request_id');
    }
}
