<?php


namespace app\controllers\api;

use app\models\Transfer;
use Yii;
use yii\rest\Controller;
use app\models\Request;

class TransferController extends Controller {

	public function actionUpdateStatus() {
		if (Yii::$app->request->isAjax) {
			$data = Yii::$app->request->post();
			$transfer = Transfer::findOne($data['id']);
			$request = Request::findOne($transfer->request_id);
			if($data['status'] == Transfer::APPROVE) {

				$request->status =Request::DENY;
				$request->save();

				$transfer->status =Transfer::DENY;
				$transfer->save();

			} else {
				$request->status = Request::APPROVE ;
				$request->save();

				$transfer->status =Transfer::APPROVE;
				$transfer->save();
			}


    }

	}
}
