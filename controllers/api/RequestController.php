<?php


namespace app\controllers\api;

use Yii;
use yii\rest\Controller;
use app\models\Request;
use app\models\Transfer;

class RequestController extends Controller {

	public function actionUpdateStatus() {
		if (Yii::$app->request->isAjax) {
			$data = Yii::$app->request->post();
			$request = Request::findOne($data['id']);
			$transfer = Transfer::findOne(['request_id' => $request->id]);
			if($data['status'] == Request::APPROVE) {

				$request->status = Request::DENY ;
				$request->save();

				$transfer->status = Transfer::DENY ;
				$transfer->save();


			} else {
				$request->status = Request::APPROVE;
				$request->save();

				$transfer->status = Transfer::APPROVE ;
				$transfer->save();
			}


    }

	}
}
