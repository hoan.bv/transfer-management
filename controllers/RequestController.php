<?php

namespace app\controllers;

use app\models\Employee;
use Yii;
use app\models\Request;
use app\models\SearchRequest;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use ZMQ;
use ZMQContext;
use WebSocket\Client;

/**
 * RequestController implements the CRUD actions for Request model.
 */
class RequestController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
	            'class' => AccessControl::className(),
	            'only' => ['index', 'create', 'update', 'delete'],
	            'rules' => [
		            [
			            'allow' => true,
			            'roles' => ['@'],
		            ],
	            ],
            ],
        ];
    }

    /**
     * Lists all Request models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchRequest();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	    $locations   = \yii\helpers\ArrayHelper::map(\app\models\Location::find()->all(), 'id', 'name');
	    $projects    = \yii\helpers\ArrayHelper::map(\app\models\Project::find()->all(), 'id', 'project_name');
	    $departments = \yii\helpers\ArrayHelper::map(\app\models\Department::find()->all(), 'id', 'department_name');
	    $employees   = \yii\helpers\ArrayHelper::map(\app\models\Employee::find()->all(), 'id', 'employee_name');
	    $status      = \app\models\Transfer::STATUS;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'locations'   => $locations,
            'projects'    => $projects,
            'departments' => $departments,
            'employees'   => $employees,
            'status'     => $status,
        ]);
    }

    /**
     * Displays a single Request model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Request model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Request();
	    if ($model->load(Yii::$app->request->post())  ) {
	    	if( \app\models\User::USER) {
	    		$model -> employee_id = Employee::findOne(Yii::$app->user->getId())->id;
		    }
	    	if($model->save()) {
			    $client = new Client("ws://localhost:8000/");
			    $client->text('New Request created with ID: '.$model->getPrimaryKey());
			    //		echo $client->receive();
			    $client->close();

			    return $this->redirect(['view', 'id' => $model->id]);
		    }
        }
	    $pending     = \app\models\Request::PENDING;
	    $locations   = \yii\helpers\ArrayHelper::map(\app\models\Location::find()->all(), 'id', 'name');
	    $projects    = \yii\helpers\ArrayHelper::map(\app\models\Project::find()->all(), 'id', 'project_name');
	    $departments = \yii\helpers\ArrayHelper::map(\app\models\Department::find()->all(), 'id', 'department_name');
	    $employees   = \yii\helpers\ArrayHelper::map(\app\models\Employee::find()->all(), 'id', 'employee_name');
	    $status      = \app\models\Transfer::STATUS;
        return $this->render('create', [
	        'model'       => $model,
	        'locations'   => $locations,
	        'projects'    => $projects,
	        'departments' => $departments,
	        'employees'   => $employees,
	        'status'      => $status,
	        'pending'     => $pending
        ]);
    }

    /**
     * Updates an existing Request model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if($model->status != Request::PENDING) {
	        Yii::$app->session->setFlash('danger', " Can't update, Request status is not  Pending");
	        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
	    $locations   = \yii\helpers\ArrayHelper::map(\app\models\Location::find()->all(), 'id', 'name');
	    $projects    = \yii\helpers\ArrayHelper::map(\app\models\Project::find()->all(), 'id', 'project_name');
	    $departments = \yii\helpers\ArrayHelper::map(\app\models\Department::find()->all(), 'id', 'department_name');
	    $employees   = \yii\helpers\ArrayHelper::map(\app\models\Employee::find()->all(), 'id', 'employee_name');
	    $status      = \app\models\Transfer::STATUS;
	    $pending     = \app\models\Request::PENDING;
        return $this->render('update', [
	        'model'       => $model,
	        'locations'   => $locations,
	        'projects'    => $projects,
	        'departments' => $departments,
	        'employees'   => $employees,
	        'status'      => $status,
	        'pending'     => $pending
        ]);
    }

    /**
     * Deletes an existing Request model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
	    if($model->status != Request::PENDING) {
		    Yii::$app->session->setFlash('danger', "Can't delete, Request status is not  Pending");
		    return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
	    }
		$model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Request model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Request the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Request::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
