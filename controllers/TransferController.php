<?php

namespace app\controllers;

use Yii;
use app\models\Transfer;
use app\models\SearchTransfer;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TransferController implements the CRUD actions for Transfer model.
 */
class TransferController extends Controller {

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index', 'create', 'update', 'delete'],
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * Lists all Transfer models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new SearchTransfer();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		Url::remember();
		$locations   = \yii\helpers\ArrayHelper::map(\app\models\Location::find()->all(), 'id', 'name');
		$projects    = \yii\helpers\ArrayHelper::map(\app\models\Project::find()->all(), 'id', 'project_name');
		$departments = \yii\helpers\ArrayHelper::map(\app\models\Department::find()->all(), 'id', 'department_name');
		$employees   = \yii\helpers\ArrayHelper::map(\app\models\Employee::find()->all(), 'id', 'employee_name');
		$status      = \app\models\Transfer::STATUS;
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'locations'    => $locations,
			'projects'     => $projects,
			'departments'  => $departments,
			'employees'    => $employees,
			'status'       => $status,
		]);
	}

	/**
	 * Displays a single Transfer model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Transfer model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model       = new Transfer();
		$locations   = \yii\helpers\ArrayHelper::map(\app\models\Location::find()->all(), 'id', 'name');
		$projects    = \yii\helpers\ArrayHelper::map(\app\models\Project::find()->all(), 'id', 'project_name');
		$departments = \yii\helpers\ArrayHelper::map(\app\models\Department::find()->all(), 'id', 'department_name');
		$employees   = \yii\helpers\ArrayHelper::map(\app\models\Employee::find()->all(), 'id', 'employee_name');
		$status      = \app\models\Transfer::STATUS;
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		}
		return $this->render('create', [
			'model'       => $model,
			'locations'   => $locations,
			'projects'    => $projects,
			'departments' => $departments,
			'employees'   => $employees,
			'status'      => $status,
		]);
	}

	/**
	 * Updates an existing Transfer model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id) {
		$model       = $this->findModel($id);
		$locations   = \yii\helpers\ArrayHelper::map(\app\models\Location::find()->all(), 'id', 'name');
		$projects    = \yii\helpers\ArrayHelper::map(\app\models\Project::find()->all(), 'id', 'project_name');
		$departments = \yii\helpers\ArrayHelper::map(\app\models\Department::find()->all(), 'id', 'department_name');
		$employees   = \yii\helpers\ArrayHelper::map(\app\models\Employee::find()->all(), 'id', 'employee_name');
		$status      = \app\models\Transfer::STATUS;
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		}
		return $this->render('update', [
			'model'       => $model,
			'locations'   => $locations,
			'projects'    => $projects,
			'departments' => $departments,
			'employees'   => $employees,
			'status'      => $status,
		]);
	}

	/**
	 * Deletes an existing Transfer model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the Transfer model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Transfer the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Transfer::findOne($id)) !== null) {
			return $model;
		}
		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
