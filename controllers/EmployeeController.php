<?php

namespace app\controllers;

use app\models\User;
use Yii;
use app\models\Employee;
use app\models\SearchEmployee;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmployeeController implements the CRUD actions for Employee model.
 */
class EmployeeController extends Controller {

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'only'  => [
					'index',
					'create',
					'update',
					'delete',
				],
				'rules' => [
					[
						'actions'       => [
							'index',
							'create',
							'update',
							'delete',
						],
						'allow'         => true,
						'roles'         => ['@'],
						'matchCallback' => function($rule, $action) {
							return Yii::$app->user->identity->role == User::ADMIN;
						},
					],
				],
			],
		];
	}

	/**
	 * Lists all Employee models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new SearchEmployee();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$model        = new Employee();
		$locations    = \yii\helpers\ArrayHelper::map(\app\models\Location::find()->all(), 'id', 'name');
		$projects     = \yii\helpers\ArrayHelper::map(\app\models\Project::find()->all(), 'id', 'project_name');
		$departments  = \yii\helpers\ArrayHelper::map(\app\models\Department::find()->all(), 'id', 'department_name');
		$sex          = \app\models\Employee::SEX;
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'locations'    => $locations,
			'projects'     => $projects,
			'departments'  => $departments,
			'sex'          => $sex,
		]);
	}

	/**
	 * Displays a single Employee model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Employee model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model       = new Employee();
		$locations   = \yii\helpers\ArrayHelper::map(\app\models\Location::find()->all(), 'id', 'name');
		$projects    = \yii\helpers\ArrayHelper::map(\app\models\Project::find()->all(), 'id', 'project_name');
		$departments = \yii\helpers\ArrayHelper::map(\app\models\Department::find()->all(), 'id', 'department_name');
		$user        = \yii\helpers\ArrayHelper::map(\app\models\User::find()->all(), 'id', 'id');
		$status      = \app\models\User::ROLE;
		$sex         = \app\models\Employee::SEX;
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		}
		return $this->render('create', [
			'model'       => $model,
			'locations'   => $locations,
			'projects'    => $projects,
			'departments' => $departments,
			'user'        => $user,
			'status'      => $status,
			'sex'         => $sex,
		]);
	}

	/**
	 * Updates an existing Employee model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id) {
		$model       = $this->findModel($id);
		$locations   = \yii\helpers\ArrayHelper::map(\app\models\Location::find()->all(), 'id', 'name');
		$projects    = \yii\helpers\ArrayHelper::map(\app\models\Project::find()->all(), 'id', 'project_name');
		$departments = \yii\helpers\ArrayHelper::map(\app\models\Department::find()->all(), 'id', 'department_name');
		$user        = \yii\helpers\ArrayHelper::map(\app\models\User::find()->all(), 'id', 'id');
		$status      = \app\models\User::ROLE;
		$sex         = \app\models\Employee::SEX;
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		}
		return $this->render('update', [
			'model'       => $model,
			'locations'   => $locations,
			'projects'    => $projects,
			'departments' => $departments,
			'user'        => $user,
			'status'      => $status,
			'sex'         => $sex,
		]);
	}

	/**
	 * Deletes an existing Employee model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the Employee model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Employee the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Employee::findOne($id)) !== null) {
			return $model;
		}
		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
