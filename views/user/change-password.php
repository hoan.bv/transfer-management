<?php
/**
 * Created by Navatech.
 * @project transfer-management
 * @author  hoan
 * @email   hoannn.bv[at]gmail.com
 * @date    2/2/2021
 * @time    3:58 PM
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\User */
?>
<div class="user-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'username')->textInput(['readonly'=> true]) ?>

	<?= $form->field($model, 'old_password')->passwordInput([ 'value' => '']) ?>

	<?= $form->field($model, 'new_password')->passwordInput(['maxlength' => true]) ?>

	<?php if( Yii::$app->user->identity->role == User::ADMIN) {

		echo $form->field($model, 'role')->dropDownList([1 => 'Admin', 2 => 'User'], ['prompt'=>'Select Role']) ;

	}?>

	<div class="form-group">
		<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
