<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\User;
//echo '<pre>';
//print_r(Yii::$app->user->identity->role == User::ADMIN);
//die;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

	<?php if( Yii::$app->user->identity->role == User::ADMIN) {

    echo $form->field($model, 'role')->dropDownList([1 => 'Admin', 2 => 'User'], ['prompt'=>'Select Role']) ;

	}?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
