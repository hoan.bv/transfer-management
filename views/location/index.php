<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchLocation */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Locations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="location-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Create Location', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'id',
			'name',
			'description',
			[
				'attribute' => 'created_at',
				'value'     => function(\app\models\Location $data) {
					return date('d-m-Y', $data->created_at);
				},
			],
			//            'updated_at',
			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
