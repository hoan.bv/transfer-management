<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */
/* @var $locations app\models\Location */
/* @var $projects app\models\Project */
/* @var $departments app\models\Department */
/* @var $employees app\models\Employee */
/* @var $status app\models\Transfer */
/* @var $user app\models\User */
/* @var $sex app\models\Employee */
$this->title                   = 'Create Employee';
$this->params['breadcrumbs'][] = [
	'label' => 'Employees',
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model'       => $model,
		'locations'   => $locations,
		'projects'    => $projects,
		'departments' => $departments,
		'user'        => $user,
		'status'      => $status,
		'sex'         => $sex,
	]) ?>

</div>
