<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */
/* @var $form yii\widgets\ActiveForm */
/* @var $locations app\models\Location */
/* @var $projects app\models\Project */
/* @var $departments app\models\Department */
/* @var $employees app\models\Employee */
/* @var $status app\models\Transfer */
/* @var $user app\models\User */
/* @var $sex app\models\Employee */
?>

<div class="employee-form">

	<div class="row">
		<?php $form = ActiveForm::begin(); ?>
		<div class="col-sm-3">
			<?= $form->field($model, 'employee_name')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'user_id')->widget(Select2::class, [
				'data'          => $user,
				'options'       => ['placeholder' => 'Select ID user ...'],
				'pluginOptions' => [
					'allowClear' => true,
				],
			]) ?></div>
		<div class="col-sm-3">

			<?= $form->field($model, 'role')->widget(Select2::class, [
				'data'          => $status,
				'options'       => ['placeholder' => 'Select status...'],
				'pluginOptions' => [
					'allowClear' => true,
				],
			]) ?>
		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'current_project_id')->widget(Select2::class, [
				'data'          => $projects,
				'options'       => ['placeholder' => 'Select project ...'],
				'pluginOptions' => [
					'allowClear' => true,
				],
			]) ?>
		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'current_location_id')->widget(Select2::class, [
				'data'          => $locations,
				'options'       => ['placeholder' => 'Select location ...'],
				'pluginOptions' => [
					'allowClear' => true,
				],
			]) ?>
		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'current_department_id')->widget(Select2::class, [
				'data'          => $departments,
				'options'       => ['placeholder' => 'Select a user ...'],
				'pluginOptions' => [
					'allowClear' => true,
				],
			]) ?>
		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'employee_first_name')->textInput(['maxlength' => true]) ?>
		</div>

		<div class="col-sm-3">
			<?= $form->field($model, 'sex')->widget(Select2::class, [
				'data'          => $sex,
				'options'       => ['placeholder' => 'Select gender ...'],
				'pluginOptions' => [
					'allowClear' => true,
				],
			]) ?>
		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'day_of_birth')->widget(DatePicker::class, [
				'options'       => [
					'placeholder' => 'Joining Date ...',
					'value'       => date('d-m-Y', ($model->day_of_birth) ? $model->day_of_birth : time()),
				],
				'pluginOptions' => [
					'autoclose' => true,
					'format'    => 'dd-mm-yyyy',
				],
			]) ?>
		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'phone_num')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>
		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'work_experience')->textarea(['rows' => 6]) ?>
		</div>
		<div class="col-sm-3">
			<div class="form-group">
				<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
			</div>

			<?php ActiveForm::end(); ?>
		</div>

	</div>
