<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchEmployee */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $locations app\models\Location */
/* @var $projects app\models\Project */
/* @var $departments app\models\Department */
/* @var $sex app\models\User */
$this->title                   = 'Employees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Create Employee', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'id',
			'employee_name',
			'user_id',
			[
				'attribute' => 'role',
				'value'     => function(\app\models\Employee $data) {
					return $data->role == \app\models\User::ADMIN ? 'Admin' : 'User';
				},
			],
			'work_experience:ntext',
			[
				'attribute'           => 'current_project_id',
				'contentOptions'      => ['class' => 'text-center'],
				'class'               => \kartik\grid\DataColumn::class,
				'filter'              => $projects,
				'filterType'          => GridView::FILTER_SELECT2,
				'filterWidgetOptions' => [
					'options'       => ['prompt' => 'Select a project ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
				'value'               => function(\app\models\Employee $data) {
					return $data->currentProject->project_name;
				},
			],
			[
				'attribute'           => 'current_location_id',
				'class'               => \kartik\grid\DataColumn::class,
				'filter'              => $locations,
				'filterType'          => GridView::FILTER_SELECT2,
				'filterWidgetOptions' => [
					'options'       => ['prompt' => 'Select a project ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
				'value'               => function(\app\models\Employee $data) {
					return $data->currentLocation->name;
				},
			],
			[
				'attribute'           => 'current_department_id',
				'class'               => \kartik\grid\DataColumn::class,
				'filter'              => $departments,
				'filterType'          => GridView::FILTER_SELECT2,
				'filterWidgetOptions' => [
					'options'       => ['prompt' => 'Select a department ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
				'value'               => function(\app\models\Employee $data) {
					return $data->currentDepartment->department_name;
				},
			],
			'employee_first_name',
			[
				'attribute' => 'day_of_birth',
				'value'     => function(\app\models\Employee $data) {
					return date('d-m-Y', $data->day_of_birth);
				},
			],
			[
				'attribute'           => 'sex',
				'class'               => \kartik\grid\DataColumn::class,
				'filter'              => $sex,
				'filterType'          => GridView::FILTER_SELECT2,
				'filterWidgetOptions' => [
					'options'       => ['prompt' => 'Select gender ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
				'value'               => function(\app\models\Employee $data) {
					if ($data->sex == \app\models\Employee::MALE) {
						return 'Male';
					} elseif ($data->sex == \app\models\Employee::FEMALE) {
						return 'Female';
					} elseif ($data->sex == \app\models\Employee::GAY) {
						return 'Gay';
					} elseif ($data->sex == \app\models\Employee::GAY) {
						return 'Less';
					}
				},
			],
			'address:ntext',
			'phone_num',
			//'created_at',
			//'updated_at',
			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
