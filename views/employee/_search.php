<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SearchEmployee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employee-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'employee_name') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'role') ?>

    <?= $form->field($model, 'work_experience') ?>

    <?php // echo $form->field($model, 'current_project_id') ?>

    <?php // echo $form->field($model, 'current_location_id') ?>

    <?php // echo $form->field($model, 'current_department_id') ?>

    <?php // echo $form->field($model, 'employee_first_name') ?>

    <?php // echo $form->field($model, 'day_of_birth') ?>

    <?php // echo $form->field($model, 'sex') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'phone_num') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
