<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Employee */
$this->title                   = $model->id;
$this->params['breadcrumbs'][] = [
	'label' => 'Employees',
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="employee-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Update', [
			'update',
			'id' => $model->id,
		], ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Delete', [
			'delete',
			'id' => $model->id,
		], [
			'class' => 'btn btn-danger',
			'data'  => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method'  => 'post',
			],
		]) ?>
	</p>

	<?= DetailView::widget([
		'model'      => $model,
		'attributes' => [
			'id',
			'employee_name',
			'user_id',
			[
				'attribute' => 'role',
				'value'     => function(\app\models\Employee $data) {
					return $data->role == \app\models\User::ADMIN ? 'Admin' : 'User';
				},
			],
			'work_experience:ntext',
			[
				'attribute' => 'current_project_id',
				'value'     => $model->currentProject->project_name,
			],
			[
				'attribute' => 'current_department_id',
				'value'     => $model->currentDepartment->department_name,
			],
			[
				'attribute' => 'current_location_id',
				'value'     => $model->currentLocation->name,
			],
			'employee_first_name',
			[
				'attribute' => 'day_of_birth',
				'value'     => function(\app\models\Employee $data) {
					return date('d-m-Y', $data->day_of_birth);
				},
			],
			[
				'attribute' => 'sex',
				'value'     => function($model) {
					if ($model->sex == \app\models\Employee::MALE) {
						return 'Male';
					} elseif ($model->sex == \app\models\Employee::FEMALE) {
						return 'Female';
					} elseif ($model->sex == \app\models\Employee::GAY) {
						return 'Gay';
					} elseif ($model->sex == \app\models\Employee::LESS) {
						return 'Less';
					}
				},
			],
			'address:ntext',
			'phone_num',
			//            'created_at',
			//            'updated_at',
		],
	]) ?>

</div>
