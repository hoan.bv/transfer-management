<?php

use kartik\grid\DataColumn;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchTransfer */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $locations app\models\Location */
/* @var $projects app\models\Project */
/* @var $departments app\models\Department */
/* @var $employees app\models\Employee */
/* @var $status app\models\Transfer */
$this->title                   = 'Transfers';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="transfer-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Create Transfer', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn',],
			//            'id',
			[
				'attribute'           => 'employee_id',
				'contentOptions'      => ['class' => 'text-center'],
				'headerOptions'       => ['class' => 'text-center'],
				'class'               => \kartik\grid\DataColumn::class,
				'filterType'          => GridView::FILTER_SELECT2,
				'filter'              => $employees,
				'filterWidgetOptions' => [
					'options'       => ['prompt' => 'Select a user ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
				'value'               => function(\app\models\Transfer $data) {
					return $data->employee->employee_name;
				},
			],
			'request_id',
			[
				'attribute'           => 'transfer_to_project_id',
				'contentOptions'      => ['class' => 'text-center'],
				'headerOptions'       => ['class' => 'text-center'],
				'class'               => \kartik\grid\DataColumn::class,
				'filterType'          => GridView::FILTER_SELECT2,
				'filter'              => $projects,
				'filterWidgetOptions' => [
					'options'       => ['prompt' => 'Select a project ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
				'value'               => function(\app\models\Transfer $data) {
					return !empty($data->toProject->project_name) ? $data->toProject->project_name : '';
				},
			],
			[
				'attribute'           => 'transfer_to_location_id',
				'contentOptions'      => ['class' => 'text-center'],
				'headerOptions'       => ['class' => 'text-center'],
				'class'               => \kartik\grid\DataColumn::class,
				'filterType'          => GridView::FILTER_SELECT2,
				'filter'              => $locations,
				'filterWidgetOptions' => [
					'options'       => [
						'prompt' => 'Select a location ...',
					],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
				'value'               => function(\app\models\Transfer $data) {
					return !empty($data->toLocation->name) ? $data->toLocation->name : '';
				},
			],
			[
				'attribute'           => 'transfer_to_department_id',
				'contentOptions'      => ['class' => 'text-center'],
				'headerOptions'       => ['class' => 'text-center'],
				'class'               => \kartik\grid\DataColumn::class,
				'filterType'          => GridView::FILTER_SELECT2,
				'filter'              => $departments,
				'filterWidgetOptions' => [
					'options'       => ['prompt' => 'Select a department ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
				'value'               => function(\app\models\Transfer $data) {
					return !empty($data->toDepartment->department_name) ? $data->toDepartment->department_name : '';
//					return $data->toDepartment->department_name;
				},
			],
			[
				'attribute'           => 'transfer_from_project_id',
				'contentOptions'      => ['class' => 'text-center'],
				'headerOptions'       => ['class' => 'text-center'],
				'class'               => \kartik\grid\DataColumn::class,
				'filterType'          => GridView::FILTER_SELECT2,
				'filter'              => $projects,
				'value'               => function(\app\models\Transfer $data) {
					return $data->fromProject->project_name;
				},
				'filterWidgetOptions' => [
					'options'       => ['prompt' => 'Select a project ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
			],
			[
				'attribute'           => 'transfer_from_location_id',
				'contentOptions'      => ['class' => 'text-center'],
				'headerOptions'       => ['class' => 'text-center'],
				'class'               => \kartik\grid\DataColumn::class,
				'filterType'          => GridView::FILTER_SELECT2,
				'filter'              => $locations,
				'value'               => function(\app\models\Transfer $data) {
					return $data->fromLocation->name;
				},
				'filterWidgetOptions' => [
					'options'       => ['prompt' => 'Select a location ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
			],
			[
				'attribute'           => 'transfer_from_department_id',
				'contentOptions'      => ['class' => 'text-center'],
				'headerOptions'       => ['class' => 'text-center'],
				'class'               => \kartik\grid\DataColumn::class,
				'filterType'          => GridView::FILTER_SELECT2,
				'filter'              => $locations,
				'filterWidgetOptions' => [
					'options'       => ['prompt' => 'Select a department ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
				'value'               => function(\app\models\Transfer $data) {
					return $data->employee->currentDepartment->department_name;
				},
			],
			[
				'class'               => DataColumn::class,
				'attribute'           => 'transfer_joining_date',
				'filterType'          => GridView::FILTER_DATE_RANGE,
				'format'              => [
					'date',
					'php:d-m-Y',
				],
				'filterWidgetOptions' => [
					'readonly'      => 'readonly',
					'convertFormat' => true,
					'pluginOptions' => [
						'locale'    => ['format' => 'd-m-Y'],
						'autoclose' => true,
					],
					'pluginEvents'  => [
						"cancel.daterangepicker" => 'function(ev,picker){$(this).val("").trigger("change");}',
					],
				],
			],
			[
				'label'               => 'Status',
				'attribute'           => 'status',
				'filter'              => $status,
				'class'               => \kartik\grid\DataColumn::class,
				'filterType'          => GridView::FILTER_SELECT2,
				'filterWidgetOptions' => [
					'options'       => ['prompt' => 'Select a stats ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
				'contentOptions'      => ['class' => 'text-center'],
				'headerOptions'       => ['class' => 'text-center'],
				'value'               => function(\app\models\Transfer $data) {
					if ($data->status == \app\models\Transfer::DENY) {
						return '<button type="button" data-status="' . $data->status . '" value="' . $data->id . '"  class="btn btn-danger check-status">Denied</button>';
					} elseif ($data->status == \app\models\Transfer::APPROVE) {
						return '<button type="button" data-status="' . $data->status . '" value="' . $data->id . '" class="btn btn-success check-status">Approved</button>';
					} elseif ($data->status == \app\models\Transfer::PENDING) {
						return '<button type="button" data-status="' . $data->status . '" value="' . $data->id . '" class="btn btn-primary  check-status">Pending</button>';
					}
				},
				'format'              => 'raw',
			],
			[
				'class'               => DataColumn::class,
				'attribute'           => 'transfer_relieving_date',
				'filterType'          => GridView::FILTER_DATE_RANGE,
				'format'              => [
					'date',
					'php:d-m-Y',
				],
				'filterWidgetOptions' => [
					'readonly'      => 'readonly',
					'convertFormat' => true,
					'pluginOptions' => [
						'locale'    => ['format' => 'd-m-Y'],
						'autoclose' => true,
					],
					'pluginEvents'  => [
						"cancel.daterangepicker" => 'function(ev,picker){$(this).val("").trigger("change");}',
					],
				],
			],
			[
				'class'               => DataColumn::class,
				'attribute'           => 'created_at',
				'filterType'          => GridView::FILTER_DATE_RANGE,
				'format'              => [
					'date',
					'php:d-m-Y',
				],
				'filterWidgetOptions' => [
					'readonly'      => 'readonly',
					'convertFormat' => true,
					'pluginOptions' => [
						'locale'    => ['format' => 'd-m-Y'],
						'autoclose' => true,
					],
					'pluginEvents'  => [
						"cancel.daterangepicker" => 'function(ev,picker){$(this).val("").trigger("change");}',
					],
				],
			],
			//'updated_at',
			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
<?php
$url     = Url::toRoute([
	'api/transfer/update-status',
]);
$pending = \app\models\Request::PENDING;
$deny    = \app\models\Request::DENY;
$approve = \app\models\Request::APPROVE;
if (Yii::$app->user->identity->role == \app\models\User::ADMIN) {
	$this->registerJs(<<<JS
	$(".check-status" ).click(function() {
		console.log(342423)
		var status = $(this);
		 var id = status.val() ;
		 var data_status = status.attr('data-status');


		$.ajax({
		  url: "$url",
		  method: "POST",
		  cache: false,
		 data: { id: id, status: data_status },
		 dataType: 'json',
		  success: function (response) {
            // var result = response.data;
            console.log(response);



             if(data_status == "$approve") {
			    console.log(1);
			    status.removeClass('btn-success').addClass('btn-danger').attr('data-status', 0).html('Denied');
			 } else if (data_status == "$deny") {
	            status.removeClass('btn-danger').addClass('btn-success').attr('data-status', 1).html('Approved');
					console.log(0);
			 } else if (data_status == "$pending") {
	            status.removeClass('btn-primary').addClass('btn-success').attr('data-status', 1).html('Approved');
					console.log(0);
        	}
		  }
		})
	});
JS
	);
}
?>
