<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Transfer */
/* @var $form yii\widgets\ActiveForm */
/* @var $locations app\models\Location */
/* @var $projects app\models\Project */
/* @var $departments app\models\Department */
/* @var $employees app\models\Employee */
/* @var $status app\models\Transfer */
?>

<div class="transfer-form">

	<?php $form = ActiveForm::begin(); ?>

	<div class="row">
		<div class="col-sm-3">
			<?= $form->field($model, 'employee_id')->widget(Select2::class, [
				'data'          => $employees,
				'options'       => ['placeholder' => 'Select a user ...'],
				'pluginOptions' => [
					'allowClear' => true,
				],
			]) ?>

		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'request_id')->textInput() ?>
		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'transfer_to_project_id')->widget(Select2::class, [
				'data'          => $projects,
				'options'       => [
					'placeholder' => 'Select a project ...',
					'class'       => 'col-xs-6',
				],
				'pluginOptions' => [
					'allowClear' => true,
				],
			]) ?>

		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'transfer_to_location_id')->widget(Select2::class, [
				'data'          => $locations,
				'options'       => ['placeholder' => 'Select a location ...'],
				'pluginOptions' => [
					'allowClear' => true,
				],
			]) ?>

		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'transfer_to_department_id')->widget(Select2::class, [
				'data'          => $departments,
				'options'       => ['placeholder' => 'Select a department ...'],
				'pluginOptions' => [
					'allowClear' => true,
				],
			]) ?>

		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'transfer_from_project_id')->widget(Select2::class, [
				'data'          => $projects,
				'options'       => [
					'placeholder' => 'Select a project ...',
					'class'       => 'col-xs-6',
				],
				'pluginOptions' => [
					'allowClear' => true,
				],
			]) ?>

		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'transfer_from_location_id')->widget(Select2::class, [
				'data'          => $locations,
				'options'       => ['placeholder' => 'Select a location ...'],
				'pluginOptions' => [
					'allowClear' => true,
				],
			]) ?>

		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'transfer_from_department_id')->widget(Select2::class, [
				'data'          => $departments,
				'options'       => ['placeholder' => 'Select a department ...'],
				'pluginOptions' => [
					'allowClear' => true,
				],
			]) ?>

		</div>
		<div class="col-sm-3">
			<?php
			if (Yii::$app->user->identity->role != \app\models\User::USER) {
				$model->status = \app\models\Transfer::PENDING;
				echo $form->field($model, 'status')->widget(Select2::class, [
					'data'          => $status,
					'options'       => ['placeholder' => 'Select status ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				]);
			} else {
				$model->status = \app\models\Transfer::PENDING;
				echo $form->field($model, 'status')->widget(Select2::class, [
					'data'          => $status,
					'options'       => ['placeholder' => 'Select status ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
					'pluginEvents' => [
						"select2:opening" => "function() { $('#transfer-status').attr('disabled', true); }",
					],
				]);
			}
			?>

		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'transfer_joining_date')->widget(DatePicker::class, [
				'model'         => $model,
				'value'         => $model->transfer_joining_date,
				'options'       => [
					'placeholder' => 'Joining Date ...',
					'value'       => date('d-m-Y', ($model->transfer_joining_date) ? $model->transfer_joining_date : time()),
				],
				'pluginOptions' => [
					'autoclose' => true,
					'format'    => 'dd-mm-yyyy',
				],
			]) ?>

		</div>
		<div class="col-sm-3">
			<?= $form->field($model, 'transfer_relieving_date')->widget(DatePicker::class, [
				'options'       => [
					'placeholder' => 'Joining Date ...',
					'value'       => date('d-m-Y', ($model->transfer_relieving_date) ? $model->transfer_relieving_date : time()),
				],
				'pluginOptions' => [
					'autoclose' => true,
					'format'    => 'dd-mm-yyyy',
				],
			]) ?>

		</div>

	</div>

	<div class="form-group">
		<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
