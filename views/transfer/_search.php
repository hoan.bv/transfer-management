<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SearchTransfer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transfer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'employee_id') ?>

	<?= $form->field($model, 'request_id') ?>

	<?= $form->field($model, 'transfer_to_project_id') ?>

    <?= $form->field($model, 'transfer_to_location_id') ?>

    <?= $form->field($model, 'transfer_to_department_id') ?>

    <?php // echo $form->field($model, 'transfer_from_project_id') ?>

    <?php // echo $form->field($model, 'transfer_from_location_id') ?>

    <?php // echo $form->field($model, 'transfer_from_department_id') ?>

    <?php // echo $form->field($model, 'transfer_joining_date') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'transfer_relieving_date') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
