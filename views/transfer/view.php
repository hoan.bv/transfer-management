<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Transfer */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Transfers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="transfer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
	        [
		        'attribute' => 'employee_id',
		        'value' => $model->employee->employee_name,
	        ],
	        'request_id',
	        [
		        'attribute' => 'transfer_to_project_id',
		        'value' => $model->toProject->project_name,
	        ],
	        [
		        'attribute' => 'transfer_from_project_id',
		        'value' => $model->fromProject->project_name,
	        ],
	        [
		        'attribute' => 'transfer_from_location_id',
		        'value' => $model->fromLocation->name,
	        ], [
		        'attribute' => 'transfer_to_location_id',
		        'value' => $model->toLocation->name,
	        ],[
		        'attribute' => 'transfer_to_department_id',
		        'value' => $model->toDepartment->department_name,
	        ],[
		        'attribute' => 'transfer_from_department_id',
		        'value' => $model->fromDepartment->department_name,
	        ],
	        [
		        'attribute' => 'transfer_joining_date',
		        'value' => date('d-m-Y', $model->transfer_joining_date),
	        ],
            [
                'attribute' => 'status',
                'value' => function($model) {
	                if($model->status == \app\models\Transfer::APPROVE) {
						return 'Approve';
	                }elseif($model->status == \app\models\Transfer::DENY) {
		                return 'Deny';
	                }elseif($model->status == \app\models\Transfer::PENDING) {
		                return 'Pending';
	                }
                }
            ],
	        [
		        'attribute' => 'transfer_relieving_date',
		        'value' => date('d-m-Y', $model->transfer_relieving_date),
	        ],
//            'created_at',
//            'updated_at',
        ],
    ]) ?>

</div>
