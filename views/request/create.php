<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Request */
/* @var $locations app\models\Location */
/* @var $projects app\models\Project */
/* @var $departments app\models\Department */
/* @var $employees app\models\Employee */
/* @var $status app\models\Transfer */
/* @var $pending app\controllers\RequestController */

$this->title = 'Create Request';
$this->params['breadcrumbs'][] = ['label' => 'Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
	    'model'       => $model,
	    'locations'   => $locations,
	    'projects'    => $projects,
	    'departments' => $departments,
	    'employees'   => $employees,
	    'status'      => $status,
	    'pending'     => $pending
    ]) ?>

</div>
