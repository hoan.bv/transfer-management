<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Request */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="request-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
	        [
		        'label' => 'Name Employee',
		        'attribute' => 'employee_id',
		        'value' => $model->employee->employee_name,
	        ],[
		        'label' => 'Project',
		        'attribute' => 'project_id',
		        'value' => $model->project->project_name,
	        ],[
		        'label' => 'Location',
		        'attribute' => 'location_id',
		        'value' => $model->location->name,
	        ],[
		        'label' => 'Department',
		        'attribute' => 'department_id',
		        'value' => $model->department->department_name,
	        ],[
		        'label' => 'Status',
		        'attribute' => 'status',
		        'value' => $model->status == 1 ? 'Approved' : 'Denied',
	        ],
            'status',
            'content:ntext',
	        [
		        'label' => 'Created At',
		        'attribute' => 'created_at',
		        'value' => date('H:i:s d-m-Y', $model->created_at),
	        ],[
		        'label' => 'Updated At',
		        'attribute' => 'updated_at',
		        'value' => date('H:i:s d-m-Y', $model->updated_at),
	        ],
        ],
    ]) ?>

</div>
