<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Request */
/* @var $form yii\widgets\ActiveForm */
/* @var $locations app\models\Location */
/* @var $projects app\models\Project */
/* @var $departments app\models\Department */
/* @var $employees app\models\Employee */
/* @var $status app\models\Transfer */
/* @var $pending app\controllers\RequestController */
?>
<?php

?>
<div class="request-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
	    <?php
	    if(Yii::$app->user->identity->role != \app\models\User::USER) {
	    ?>
	    <div class="col-sm-3">
		    <?= $form->field($model, 'employee_id')->widget(Select2::class, [
			    'data' => $employees,
			    'options' => ['placeholder' => 'Select a user ...'],
			    'pluginOptions' => [
				    'allowClear' => true,
			    ],
		    ]) ?>
	    </div>
	    <?php
	    }
	    ?>
        <div class="col-sm-3">
            <?= $form->field($model, 'department_id')->widget(Select2::class, [
                'data' => $departments,
                'options' => ['placeholder' => 'Select a department ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'location_id')->widget(Select2::class, [
                'data' => $locations,
                'options' => ['placeholder' => 'Select a location ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'project_id')->widget(Select2::class, [
                'data' => $projects,
                'options' => ['placeholder' => 'Select a project ...', 'class' => 'col-xs-6'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
        </div>
        <div class="col-sm-3">
	        <?php
	        if (Yii::$app->user->identity->role != \app\models\User::USER) {
		        $model->status = $pending;
		        echo $form->field($model, 'status')->widget(Select2::class, [
			        'data'          => $status,
			        'options'       => ['placeholder' => 'Select status ...'],
			        'pluginOptions' => [
				        'allowClear' => true,
			        ],
		        ]);
	        } else {
		        $model->status = $pending;
		        echo $form->field($model, 'status')->widget(Select2::class, [
			        'data'          => [$pending => 'Pending'],
			        'options'       => ['placeholder' => 'Select status ...', 'readonly' => 'readonly'],
			        'pluginOptions' => [
				        'allowClear' => true,
			        ],
		        ]);
	        }
	        ?>
        </div>
	    <div class="col-sm-6">
		    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>
	    </div>
    </div>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
