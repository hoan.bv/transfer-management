<?php

use kartik\grid\DataColumn;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchRequest */
/* @var $dataProvider yii\data\ActiveDataProvider *//* @var $locations app\models\Location */
/* @var $projects app\models\Project */
/* @var $departments app\models\Department */
/* @var $employees app\models\Employee */
/* @var $status app\models\Transfer */
$this->title                   = 'Requests';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="request-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Create Request', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			[
				'attribute'      => 'id',
				'contentOptions' => ['class' => 'text-center'],
				'headerOptions'  => ['class' => 'text-center'],
				'value'          => function(\app\models\Request $data) {
					return $data->id;
				},
			],
			[
				'attribute'      => 'employee_id',
				'contentOptions' => ['class' => 'text-center'],
				'headerOptions'  => ['class' => 'text-center'],
				'class'          => \kartik\grid\DataColumn::class,
				'filterType'     => GridView::FILTER_SELECT2,
				'filter'         => $employees,
				'filterWidgetOptions' => [
					'options' => ['prompt' => 'Select a user ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
				'value'          => function(\app\models\Request $data) {
					return $data->employee->employee_name;
				},
			],
			[
				'attribute'      => 'project_id',
				'contentOptions' => ['class' => 'text-center'],
				'headerOptions'  => ['class' => 'text-center'],
				'class'          => \kartik\grid\DataColumn::class,
				'filterType'     => GridView::FILTER_SELECT2,
				'filter'         => $projects,
				'filterWidgetOptions' => [
					'options' => ['prompt' => 'Select a project ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
				'label'          => 'Project',
				'value'          => function(\app\models\Request $data) {
					return $data->project->project_name;
				},
			],
			[
				'class'          => \kartik\grid\DataColumn::class,
				'filterType'     => GridView::FILTER_SELECT2,
				'filter'         => $locations,
				'filterWidgetOptions' => [
					'options' => ['prompt' => 'Select a location ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
				'attribute'      => 'location_id',
				'contentOptions' => ['class' => 'text-center'],
				'headerOptions'  => ['class' => 'text-center'],
				'label'          => 'Location',
				'value'          => function(\app\models\Request $data) {
					return $data->location->name;
				},
			],
			[
				'attribute'      => 'department_id',
				'contentOptions' => ['class' => 'text-center'],
				'headerOptions'  => ['class' => 'text-center'],
				'label'          => 'Department',
				'class'          => \kartik\grid\DataColumn::class,
				'filterType'     => GridView::FILTER_SELECT2,
				'filter'         => $departments,
				'filterWidgetOptions' => [
					'options' => ['prompt' => 'Select a department ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
				'value'          => function(\app\models\Request $data) {
					return $data->department->department_name;
				},
			],
			[
				'label'          => 'Status',
				'attribute'      => 'status',
				'filter'         => $status,
				'class'          => \kartik\grid\DataColumn::class,
				'filterType'     => GridView::FILTER_SELECT2,
				'filterWidgetOptions' => [
					'options' => ['prompt' => 'Select a department ...'],
					'pluginOptions' => [
						'allowClear' => true,
					],
				],
				'contentOptions' => ['class' => 'text-center'],
				'headerOptions'  => ['class' => 'text-center'],
				'value'          => function(\app\models\Request $data) {
					if($data->status == \app\models\Request::DENY) {
						return '<button type="button" data-status="' . $data->status . '" value="' . $data->id . '"  class="btn btn-danger check-status">Denied</button>';
					} elseif ($data->status == \app\models\Request::APPROVE) {
						return '<button type="button" data-status="' . $data->status . '" value="' . $data->id . '" class="btn btn-success check-status">Approved</button>';
					} elseif ($data->status == \app\models\Request::PENDING) {
						return '<button type="button" data-status="' . $data->status . '" value="' . $data->id . '" class="btn btn-primary  check-status">Pending</button>';
					}

				},
				'format'         => 'raw',
			],
			'content:text',
			[
				'class'               => DataColumn::class,
				'attribute'           => 'created_at',
				'filterType'          => GridView::FILTER_DATE_RANGE,
				'format'              => [
					'date',
					'php:Y-m-d H:i:s',
				],
				'filterWidgetOptions' => [
					'readonly'      => 'readonly',
					'convertFormat' => true,
					'pluginOptions' => [
						'locale'    => ['format' => 'Y-m-d'],
						'autoclose' => true,
					],
					'pluginEvents'  => [
						"cancel.daterangepicker" => 'function(ev,picker){$(this).val("").trigger("change");}',
					],
				],
			],
			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>

</div>
<?php
$url = Url::toRoute([
	'api/request/update-status',
	'id' => 42,
]);
$pending = \app\models\Request::PENDING;
$deny = \app\models\Request::DENY;
$approve = \app\models\Request::APPROVE;
if(Yii::$app->user->identity->role == \app\models\User::ADMIN) {
	$this->registerJs(<<<JS
	$(".check-status" ).click(function() {
		var status = $(this);
		 var id = status.val() ;
		 var data_status = status.attr('data-status');


		$.ajax({
		  url: "$url",
		  method: "POST",
		  cache: false,
		 data: { id: id, status: data_status },
		 dataType: 'json',
		  success: function (response) {
            // var result = response.data;
            console.log(response);



             if(data_status == "$approve") {
			    console.log(1);
			    status.removeClass('btn-success').addClass('btn-danger').attr('data-status', 0).html('Denied');
			 } else if (data_status == "$deny") {
	            status.removeClass('btn-danger').addClass('btn-success').attr('data-status', 1).html('Approved');
					console.log(0);
			 } else if (data_status == "$pending") {
	            status.removeClass('btn-primary').addClass('btn-success').attr('data-status', 1).html('Approved');
					console.log(0);
        	}
		  }
		})
	});
JS
	);
}


?>
